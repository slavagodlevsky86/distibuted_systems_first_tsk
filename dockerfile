FROM python:3.11

RUN pip install Flask requests

COPY . /app

WORKDIR /app

CMD ["python", "main.py"]