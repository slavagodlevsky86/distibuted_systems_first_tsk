from flask import Flask, jsonify, request, abort
import json
import os
from time import sleep

from src.replicate import replicate_to_secondaries

import logging
from logging.handlers import RotatingFileHandler

app = Flask(__name__)


logging.basicConfig(level=logging.INFO)

file_handler = RotatingFileHandler('app.log', maxBytes=10000, backupCount=1)
file_handler.setLevel(logging.INFO)
file_handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))
app.logger.addHandler(file_handler)


messages_file = 'data/messages.json'

if not os.path.exists(messages_file):
    with open(messages_file, 'w') as file:
        json.dump([], file)

def read_messages():
    """Read messages from the file"""
    with open(messages_file, 'r') as file:
        return json.load(file)

def write_messages(messages):
    """Write messages to the file"""
    with open(messages_file, 'w') as file:
        json.dump(messages, file)


@app.route('/messages', methods=['POST', 'GET'])
def messages():
    if request.method == 'POST': 
        if os.environ.get('NODE_TYPE') == 'master': # <- check for master node
            data = request.get_json()
            message = data.get('message')
            if not message:
                abort(400, 'No message provided.')
            messages = read_messages()
            messages.append(message)
            write_messages(messages)
            app.logger.info(f'Appended message: {message}')
            replicate_to_secondaries(message, app.logger)

            return jsonify(success=True), 201
        else:
            abort(405, 'The node is read-only.')

    messages = read_messages()
    app.logger.info('Messages retrieved')
    return jsonify(messages=messages)


@app.route('/replicate', methods=['POST'])
def replicate():
    if os.environ.get('NODE_TYPE') == 'secondary':
        data = request.get_json()
        message = data.get('message')
        sleep(1) 
        messages = read_messages()
        messages.append(message)
        write_messages(messages)
        return jsonify(success=True), 201
    else:
        abort(403)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=False)
