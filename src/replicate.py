import os 
from threading import Thread, Lock
import requests
from flask import abort

acknowledgments = 0
ack_lock = Lock()


def replicate_to_secondaries(message, logger):
    global acknowledgments
    secondaries = os.environ.get('SECONDARIES').split(',')
    threads = []

    with ack_lock:
        acknowledgments = 0

    for secondary in secondaries:
        thread = Thread(target=send_replication_request, args=(secondary, message, logger))
        threads.append(thread)
        thread.start()

    for thread in threads:
        thread.join()

    if acknowledgments != len(secondaries):
        abort(500, 'Not all secondaries acknowledged the replication.')


def send_replication_request(secondary, message, logger):
    global acknowledgments
    try:
        response = requests.post(f'http://{secondary}/replicate', json={'message': message}, timeout=5)
        if response.status_code == 201:
            with ack_lock:
                acknowledgments += 1
            logger.info(f'Successfully replicated message to {secondary}')
        else:
            logger.error(f'Failed to replicate to {secondary}: HTTP {response.status_code}')
    except requests.RequestException as e:
        logger.exception(f"Failed to replicate to {secondary}: {e}")